---
layout: post
title:  "A side project for fun"
date:   2015-02-16 
categories: mediator feature
tags: featured
---

As I am from Hong Kong and am aware of housing problem there, I am curious if there is really a best district for teenagers or elders. I downloaded population and work force data from Census and Statistical Department of Hong Kong. The most recent data that I was able to obtain, is from 2011. As I plotted the distribution on the map,  expected result showed most of the people in HK live in central Kowloon and HK Island. However, what I did not expect, was the pattern of work force and population distributions. They look pretty much the same, implying age distribution in all districts are probably the same. Is it because most work forces are living with their dependents? It would be interesting to also visualize the housing prices and office density in the future.

<a href="https://wckdouglas.files.wordpress.com/2015/02/percentage_workforce1.jpg"><img class="alignnone wp-image-8 " src="https://wckdouglas.files.wordpress.com/2015/02/percentage_workforce1.jpg?w=660" alt="percentage_workforce" width="735" height="490" /></a><a href="https://wckdouglas.files.wordpress.com/2015/02/percentage_workforce.jpg">
</a>

The scripts and datasets being used is deposited on [github](https://github.com/wckdouglas/HK-population-project)
