---
layout: default
category : pages
title: Software
permalink: /Software/
---
<main class="content" role="main">
<style>
img {
    width: 100%;
    height: auto;
}
</style>

As I do programming exclusively on RNA/DNA-seq experiments bioinformatics analysis, all of the softwares I wrote are mainly focus on this aspect.

#[fdrcontrol](https://github.com/wckdouglas/fdrcontrol.git)
This is a *R* package that I wrote to speed up FDR control in multiple hypothesis testing. Vignettes is available [here](http://rawgit.com/wckdouglas/fdrcontrol/master/vignettes/fdrcontrol.html).

<img class="alignnone wp-image-8 " src='{{ site.url }}/assets/article_images/softwares/fdrcontrol.png' width=700 height=300/> 

The package can be install via **devtools**.  

```r
devtools::install_github('wckdouglas/fdrcontrol')
```

#[fastq-tools](https://github.com/wckdouglas/fastq-tools)
This is a collection of small tools that I developed for manipulation of fastq files. A main function that I use frequenctly is the **filterFastq** program. This program takes in a fastq file and a id file, it can either extract all the sequence as recorded in the id file or filter all of them. 

```
usage: bin/filterFastq -q <fastq> -i <idFile>
[options]
	-v    inverted match (same as grep -v)
```

Another useful function is **splitFastq**. This is a program to split fastq files from next-gen sequencing libraries. Inspired by UNIX split function. This is particularly useful when a large library is needed to break down into smaller files for high-throughput computations.

```sh
usage: bin/splitFastq -i <fqfile> -n <# of record per file> -o <prefix> [-z]
[options]
-i    <fastq file> can be gzipped
-n    <number of record in each splitted file> default: 10000000
-o    <prefix>
-z    optional: gzip output
```

#[bedFileTools](https://github.com/wckdouglas/bedFileTools.git)
This is a collection of programs that I created to extract different information from [bed file](http://www.ensembl.org/info/website/upload/bed.html). 

A useful program is **bedpeTobed**, which is a prgram to convert bedpe file to bed file with:

* the leftmost start site will be used
* the rightmost start site will be used
* the strand for 1st read in the pair

usage:

```
./bin/bedpeTobed <bedpeFile> | <stdin>
```


#[filterSamFile](https://github.com/wckdouglas/filterSamFile.git)
This is a collection of programs that I created to extract different information from Sequence Alignment/Map [SAM file](https://samtools.github.io/hts-specs/SAMv1.pdf).

A useful program is **bedToJunction**. This program extracts all intron regions from a bed file.

```
suggested usage: bamtobed -i <bamfile> -cigar | ./bin/bedToJunction - > junction.bed
**** use <-> when using stdin
Needed a bed file with cigar string
output file contains six columns:
	column 1:        chromosome name
	column 2:        junction start position (1-base start pos, same as bamfile
	column 3:        junction end position (1-base start pos, same as bamfile)
	column 4:        name (ordered by chrom)
	column 5:        number of reads supporting
	column 6:        strand [+/-]
	column 7:        junction distance
```
