---
layout: default
category : pages
title: Research
permalink: /Research/
---

<main class="content" role="main">
<style>
img {
    width: 100%;
	height: auto;
}
</style>

#RNA biology
Coming soon...


#Mechanotransduction
<img src="{{ site.url }}/assets/research_images/wordcloud_mechanobiology.png" /> 

Since my sophomore year in UIUC, I started working on cellular mechanobiology in [Professor Ning Wang's lab](https://mechanical.illinois.edu/directory/faculty/nwangrw). Under the supervision of [Dr. Yeh Chuin Poh](https://www.linkedin.com/in/yehchuinpoh) and [Dr. Youhua Tan](https://sites.google.com/site/youhuatan/home1), I gained interest, knowlegde as well as experimental experiences on how physical forces manipulate cellular biochemistry. Throughout the three years in the Wang's lab, we have shown that local surface force applied through integrins was able to induce dissociation of nuclear bodies. In addition to dynamic local forces applied on cellular apical surface, we also showed that using 3D soft fibrin matrices, embryonic stem cells can be cultured into embryoid bodies with clear germ layers. Moreover, the 3D soft matrix can also be used for selecting tumorigenic cells from cell cultures. In summary, our work showed that cellular biochemical activity can be triggered by physical envrionment via cell-cell or cell-matrix interactions, and can be a direct way to facilitate the tumor and stem cell researches.

<a href="http://www.ncbi.nlm.nih.gov/pubmed/22643893" >
<img src="{{ site.url }}/assets/research_images/cajalBody.png" /> 
</a>

For my first rotation in UT-Austin, I joined the lab of [Professor Laura Suggs](http://research.bme.utexas.edu/suggs/Suggs_lab_website/The_Suggs_Lab.html) and continued mechanobiology research on cardiovascular cells. This is where I first started programming work. Using matlab, I built a software to simulate the magnitude of forces that the cells in each wells experiencing by incoporating the information of the paramagnetic beads, the magnetic strength of the magnet and experimentally-derived number of beads per cells. This consequently quantified the magnitude of physical forces needed for inducing differentiation embryoid bodies into cardiovascular progenitors.

<a href="http://www.ncbi.nlm.nih.gov/pubmed/25501004" >
<img src="{{ site.url }}/assets/research_images/guess_setup.png"  />
</a>
