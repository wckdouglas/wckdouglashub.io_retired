---
layout: default
category : pages
title: Publication
permalink: /Publication/
---

<main class="content" role="main">

* Yidan Qin, Jun Yao, <strong>Douglas C Wu</strong>, Ryan M Nottingham, Sabine Mohr, Scott Hunicke-Smith, Alan M Lambowitz (2016). High-throughput sequencing of human plasma RNA by using thermostable group II intron reverse transcriptases. <strong><em>RNA</em></strong>. Vol. 22, no. 1. [\[scripts\]](https://github.com/wckdouglas/plasmaMiRNATissues)[\[paper\]](http://rnajournal.cshlp.org/content/22/1/111.short)

* Laura R. Geuss, <strong>Douglas C. Wu</strong>, Divya Ramamoorthy, Corinne D. Alford, Laura J. Suggs (2014). Paramagnetic Beads and Magnetically Mediated Strain Enhance Cardiomyogenesis in Mouse Embryoid Bodies. <strong><em>PLoS ONE</em></strong>. Vol. 9, no. 12. [\[scripts\]](https://github.com/wckdouglas/magnetic_Script)[\[paper\]](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0113982)

* Youhua Tan, Arash Tajik, Junwei Chen, Qiong Jia, Farhan Chowdhury, Lili Wang, Junjian Chen, Shuang Zhang, Ying Hong, Haiying Yi, <strong>Douglas C. Wu</strong>, Yuejin Zhang, Fuxiang Wei, Yeh-Chuin Poh, Jihye Seong, Rishi Singh, Li-Jung Lin, Sultan Doganay, Yong Li, Haibo Jia, Taekjip Ha, Yingxiao Wang, Bo Huang, Ning Wang (2014). Matrix softness regulates plasticity of tumour-repopulating cells via H3K9 demethylation and Sox2 expression. <strong><em>Nature Communication</em></strong>. Vol. 5, no. 469 [\[paper\]](http://www.nature.com/ncomms/2014/140806/ncomms5619/full/ncomms5619.html)

* Yeh-Chuin Poh, Junwei Chen, Ying Hong, Haiying Yi, Shuang Zhang, Junjian Chen, <strong>Douglas C. Wu</strong>, Lili Wang, Qiong Jia, Rishi Singh, Wenting Yao, Youhua Tan, Arash Tajik, Tetsuya S. Tanaka, Ning Wang (2014). Generation of organized mouse germ layers from single embryonic stem cell. <strong><em>Nature Communication</em></strong>. Vol. 5, no. 4000 [\[paper\]](http://www.nature.com/ncomms/2014/140530/ncomms5000/full/ncomms5000.html)

* Yeh-Chuin Poh, Sergey P. Shevtsov, Farhan Chowdhury, <strong>Douglas C. Wu</strong>, Sungsoo Na, Miroslav Dundr, Ning Wang (2012). Dynamic force-induced direct dissociation of protein complexes in a nuclear body in living cells. <strong><em>Nature Communication</em></strong>. Vol. 3, no. 866 [\[paper\]](http://www.nature.com/ncomms/journal/v3/n5/abs/ncomms1873.html) 

* Yuhei Uda, Yeh-Chuin Poh, Farhan Chowdhury, <strong>Douglas C. Wu</strong>, Tetsuya S. Tanaka, Masaaki Sato, and Ning Wang (2011). Force via integrins but not E-cadherin decreases Oct3/4 expression in embryonic stem cells. <strong><em>Biochemical and Biophysical Research Communications</em></strong>. Vol. 415, no. 2, pp. 396-400. [\[paper\]](http://www.ncbi.nlm.nih.gov/pubmed/22037576)

* <strong>Douglas Wu</strong> and Mary Waye (2011). The relationship between MicroRNA and Tumor Suppressors, <strong><em>Tumor Suppressors</em></strong>, eds Susan D. Nguyen (Nova Science Publishers, New York), pp. 175-188.
