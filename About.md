---
layout: default
title: About
category : pages
permalink: /About/
---
<main class="content" role="main">

<p align="center">
	<img align="center" src="{{ site.url }}/assets/images/douglas.png" width=200,height=174>
</p>
I am a graduate student working on molecular biology at Institute for Cellular and Molecular Biology in The University of Texas at Austin at the [Lambowitz's lab]("https://sites.cns.utexas.edu/lambowitz"). My work is primarily focusing on RNA-seq. We use a new [reverse transcriptase](http://www.ingex.com/tgirt-kit/) as our main enzyme for this purpose. Before that, I was trained as a cell biologist under [Wang's Lab](http://mechanical.illinois.edu/directory/faculty/nwangrw) working on mechanobiology.


<center>
<img align="center" src="https://assets-cdn.github.com/images/modules/logos_page/GitHub-Mark.png" alt="" width="24" height="24">[wckdouglas](https://github.com/wckdouglas)  
<img align="center" src="https://cdn4.iconfinder.com/data/icons/black-icon-social-media/512/099375-twitter-logo-square.png" alt="" width="24" height="24" />[@wckdouglas](https://twitter.com/wckdouglas)
</center>
