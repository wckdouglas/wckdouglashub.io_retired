---
layout: default
category : pages
title: Resume
permalink: /Resume/
---

<main class="content" role="main">
A pdf version of my *CV* can be downloaded [here](https://github.com/wckdouglas/cv/raw/master/Douglas_cv.pdf).

# EDUCATION #
### The University of Texas at Austin (2013 - ) ###
* PhD student, Institute of Cellular and Molecular Biology
  * Advisors: Dr. Alan M. Lambowitz

### University of Illinois at Urbana Champaign (2009 - 2013) ###
* B.S., Biochemistry
  * Biochemistry High Distinction Award



---

# RESEARCH EXPERIENCE #
### Prof. Alan Lambowitz’s Lab ###
Institute of Cellular and molecular Biology    
The University of Texas at Austin    
Spring 2013 - present 	

* Leukemia T-cell (Jurkat cell) whole cell RNA-seq library 
* RNA-seq analysis for exosomal RNA-seq and plasma RNA-seq data [(paper)](http://rnajournal.cshlp.org/content/early/2015/11/09/rna.054809.115.long) 
* Applying mathematical models on RNA-seq data 


### Prof. Claus Wilke’s Lab ###
Institute of Cellular and molecular Biology    
The University of Texas at Austin    
Winter 2013 - present

* Bioinformatics: RNA-seq analysis on plasma RNA library



### Prof. Laura Sugg’s Lab ###
Department of Biomedical Engineering    
University of Texas at Austin    
Fall 2013    

* Computer simulation for quantifying magnetic-induced force that directs embryoid bodies into mesodermal lineages [(link)](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0113982)


### Prof. Ning Wang’s lab ###
Department of Mechanical Engineering    
University of Illinois at Urbana Champaign    
Spring 2010 - Spring 2013    

* Study force-induced biochemical changes in embryonic stem cells [(link)](http://www.sciencedirect.com/science/article/pii/S0006291X11018924)
* Examine the stress-induce disruption of cajal body using CFP-SMN, YFP-Coilin Hela cells [(link)](http://www.nature.com/ncomms/journal/v3/n5/abs/ncomms1873.html)    
* 3D-culturing melanoma B16F1 cells, tumorigenic cells, Embryonic stem cells in fibrin matrix [(link](http://www.nature.com/ncomms/2014/140530/ncomms5000/full/ncomms5000.html), [link](http://www.nature.com/ncomms/2014/140806/ncomms5619/full/ncomms5619.html)[,press)](https://news.illinois.edu/blog/view/6367/204581)


### Prof. Mary Waye’s Lab ###
Department of Biochemistry    
Chinese University of Hong Kong    
Summer 2010    

* Investigate KIAA0319 gene on suicide and musically perfect pitch using human DNA extracted from saliva as experimental subject




---


# INVITED TALK #
* Next generation sequencing of circulating RNA in plasma 	
Byte club meeting 	
The University of Texas at Austin 	
Austin, TX. 	
Feb 19, 2014

---

# Scholarship #
* Open Science Grid User Summer School Scholarship (2014) 
* University of Washington - 1st Summer Institute in Statistics for Big Data (Declined)
* University of Washington - 7th Summer Institute in Statistics and Modeling in Infectious Diseases (Declined)

---

# SKILLS #
* Languages
  * English
  * Cantonese Chinese
  * Mandarin Chinese
* Programming Languages
  * *Python*
  * *R*
  * *Bash*
  * *Matlab*, *Octave*
  * *LaTeX* 
  * *MySQL*
  * *C++*/*C*
* Working knowledge of High Performance Computing
  * SGE Batch Environment (TACC lonestar)
  * SLURM (TACC stampede, lonestar5)
* Working knowledge of High throughput Computing/ Grid computing
  * HTcondor
* Experimental techniques
  * TGIRT-seq library
  * qPCR
  * Fluorescence microscopy
  * 3D cell culture
  * Magnetic twisting cytometry
  * Live cell imaging

